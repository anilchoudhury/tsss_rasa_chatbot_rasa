#FROM rasa/rasa:latest-full 
FROM rasa/rasa:2.3.0

USER root
 
WORKDIR /app

COPY . /app

COPY ./data /app/data

COPY ./endpoints.yml /app

COPY ./credentials.yml /app

COPY ./models /app/models

RUN ls -lah /app/models

VOLUME /app
VOLUME /app/data
VOLUME /app/models

CMD [ "run","-m","/app/models","--enable-api","--cors","*","--debug","--endpoints","endpoints.yml","--credentials","credentials.yml"]
 

#USER root
#WORKDIR /app
#COPY . /app
#COPY ./data/ /app/data/
#COPY ./endpoints.yml /app
#COPY ./credentials.yml /app
#COPY ./models/20210406-201244.tar.gz /app/models/20210406-201244.tar.gz
#RUN ls -lha /app/models/
#VOLUME /app
#VOLUME /app/data/
#VOLUME /app/models/
#RUN apt-get update && apt-get -y install gcc
#CMD [ "run","-m","/app/models/","--enable-api","--cors","*","--debug","--endpoints","endpoints.yml","--credentials","credentials.yml"]
 
