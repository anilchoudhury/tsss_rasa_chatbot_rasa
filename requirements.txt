# Incase the docker isnt being used and intent of programmer is to run the project in local machine ,uncomment below stuff
# and after installation run below two statements with admin privellage


#rasa==2.4.0
#sanic==20.3.0
numpy==1.16.0
spacy==2.3.2
asyncio==3.4.3
jsons==1.2.0
fastapi==0.60.0
sqlalchemy==1.3.19
uvicorn==0.11.2
pydantic
nltk
aiocache
msgpack
python-socketio==4.4
pyjwt==2.0.0
#googletrans==4.0.0rc1
gql==3.0.0a5
PyYAML==5.4.1


